use std::collections::HashMap;

use crate::IpfsApi;

use reqwest;
use failure::Error;

use bytes::Bytes;

#[derive(Deserialize)]
struct DagPutResponse {
    #[serde(rename = "Cid")]
    cid: HashMap<String, String>,
}

impl IpfsApi {
    pub async fn dag_put<'a>(&'a self, data: Bytes) -> Result<HashMap<String,String>, Error> {
        let mut url = self.get_url()?;
        url.set_path("api/v0/dag/put");

        let res = {
            self.client.post(url)
                .multipart(reqwest::multipart::Form::new()
                    .part("arg", reqwest::multipart::Part::bytes(data.to_vec()))
                ).send().await?
        };

        let json: DagPutResponse = res.json().await?;
        Ok(json.cid)
    }

    pub async fn dag_get(&self, hash: &str) -> Result<Bytes, Error> {
        let mut url = self.get_url()?;
        url.set_path("api/v0/dag/get");
        url.query_pairs_mut()
            .append_pair("arg", hash);

        Ok(self.client.post(url).send().await?.bytes().await?)
    }
}

#[cfg(test)]
mod tests {
    // use crate::IpfsApi;

    // #[test]
    // fn test_dag_put() {
    //     let api = IpfsApi::new("127.0.0.1", 5001);

    //     let hash = api.dag_put("Hello world".as_bytes().into()).await.unwrap();

    //     assert_eq!(hash, "QmV8cfu6n4NT5xRr2AHdKxFMTZEJrA44qgrBCr739BN9Wb");
    // }

    // #[test]
    // fn test_dag_get() {
    //     let api = IpfsApi::new("127.0.0.1", 5001);

    //     let content = "Hello world\n".as_bytes();

    //     let hash = api.dag_put(content).unwrap();
    //     let dag: Vec<u8> = api.dag_get(&hash).unwrap().collect();

    //     assert_eq!(dag, content);
    // }
}